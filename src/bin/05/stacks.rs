#[derive(Clone, Debug)]
pub struct Stacks {
    stacks: Vec<Stack>,
    moves: Vec<Move>,
}
impl Stacks {
    pub fn from_string(s: String) -> Self {
        let lines = s.lines();
        let stack_strings = lines
            .clone()
            .take_while(|line| line.trim().starts_with('['))
            .map(str::to_owned)
            .collect::<Vec<_>>();
        let num_stacks = lines
            .clone()
            .skip(stack_strings.len())
            .take(1)
            .map(|s| s.split_whitespace().next_back().unwrap().parse().unwrap())
            .collect::<Vec<usize>>()[0];
        let mut stacks: Vec<Stack> = Vec::with_capacity(num_stacks);
        stacks.resize_with(num_stacks, Stack::new);
        stack_strings.iter().rev().for_each(|s| {
            let mut iter = s.chars();
            iter.next();
            let mut idx = 0;
            loop {
                if idx == num_stacks {
                    break;
                }
                if let Some(c) = Self::parse_crate(iter.next().unwrap_or(' ')) {
                    stacks[idx].push(c);
                }
                iter.next();
                iter.next();
                iter.next();
                idx += 1;
            }
        });

        let moves = lines
            .skip_while(|line| !line.starts_with("move"))
            .map(Move::from_str)
            .collect();

        Self { stacks, moves }
    }

    fn parse_crate(c: char) -> Option<char> {
        if c == ' ' {
            None
        } else {
            Some(c)
        }
    }

    pub fn apply_moves_crate_by_crate(&mut self) {
        for m in &self.moves {
            let mut to_move = self.stacks[m.from_stack - 1].pop(m.num_moves);
            to_move.reverse();
            self.stacks[m.to_stack - 1].append(&mut to_move);
        }

        self.moves.clear();
    }

    pub fn apply_moves_crate_chunks(&mut self) {
        for m in &self.moves {
            let mut to_move = self.stacks[m.from_stack - 1].pop(m.num_moves);
            self.stacks[m.to_stack - 1].append(&mut to_move);
        }

        self.moves.clear();
    }

    pub fn crates_on_top_as_string(&self) -> String {
        self.stacks
            .iter()
            .map(|stack| stack.0.last().unwrap_or(&' '))
            .collect()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Stack(Vec<char>);
impl Stack {
    fn new() -> Self {
        Self(Vec::new())
    }

    fn push(&mut self, crate_: char) {
        self.0.push(crate_);
    }

    fn append(&mut self, other: &mut Self) {
        self.0.append(&mut other.0);
    }

    fn pop(&mut self, n: usize) -> Self {
        Self(self.0.drain(self.0.len() - n..).collect())
    }

    fn reverse(&mut self) {
        self.0.reverse();
    }

    #[cfg(test)]
    fn from_array(a: &[char]) -> Self {
        Self(a.to_vec())
    }
}

#[derive(Clone, Debug, PartialEq)]
struct Move {
    num_moves: usize,
    from_stack: usize,
    to_stack: usize,
}
impl Move {
    fn from_str(s: &str) -> Self {
        let raw_action = s
            .split_whitespace()
            .filter_map(|s| s.parse().ok())
            .collect::<Vec<usize>>();
        Self {
            num_moves: raw_action[0],
            from_stack: raw_action[1],
            to_stack: raw_action[2],
        }
    }

    #[cfg(test)]
    fn from_num_from_to(num_moves: usize, from_stack: usize, to_stack: usize) -> Self {
        Self {
            num_moves,
            from_stack,
            to_stack,
        }
    }
}

#[cfg(test)]
mod stacks_tests {
    use super::*;

    #[test]
    fn stacks_from_str() {
        let s = "    [D]\n\
                 [N] [C]\n\
                 [Z] [M] [P]\n\
                  1   2   3\n\
                  \n\
                  move 1 from 2 to 1\n\
                  move 3 from 1 to 3\n\
                  move 2 form 2 to 1\n\
                  move 1 from 1 to 2\n"
            .to_string();
        let stacks = Stacks::from_string(s);

        assert_eq!(stacks.stacks[0], Stack::from_array(&['Z', 'N']));
        assert_eq!(stacks.stacks[1], Stack::from_array(&['M', 'C', 'D']));
        assert_eq!(stacks.stacks[2], Stack::from_array(&['P']));

        assert_eq!(stacks.moves[0], Move::from_num_from_to(1, 2, 1));
        assert_eq!(stacks.moves[1], Move::from_num_from_to(3, 1, 3));
        assert_eq!(stacks.moves[2], Move::from_num_from_to(2, 2, 1));
        assert_eq!(stacks.moves[3], Move::from_num_from_to(1, 1, 2));
    }
}

#[cfg(test)]
mod move_tests {
    use super::*;

    #[test]
    fn move_from_str() {
        let m = Move::from_str("move 14 from 3 to 199");

        assert_eq!(m.num_moves, 14);
        assert_eq!(m.from_stack, 3);
        assert_eq!(m.to_stack, 199);
    }
}
