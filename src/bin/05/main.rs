mod stacks;

use std::fs;

use stacks::Stacks;

const INPUT: &str = "src/bin/05/input.txt";

fn main() {
    let mut stacks = parse_input(INPUT);

    {
        let mut stacks = stacks.clone();
        stacks.apply_moves_crate_by_crate();
        let crates_on_top = stacks.crates_on_top_as_string();
        println!(
            "The crates on top of each stack after moving crate by crate are: {crates_on_top}"
        );
    }

    stacks.apply_moves_crate_chunks();
    let crates_on_top = stacks.crates_on_top_as_string();
    println!("The crates on top of each stack after moving crate chunks are: {crates_on_top}");
}

fn parse_input(path: &str) -> Stacks {
    Stacks::from_string(fs::read_to_string(path).expect("Failed to read file"))
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "src/bin/05/test_input.txt";

    #[test]
    fn move_crate_by_crate_crates_on_top() {
        let mut stacks = parse_input(TEST_INPUT);

        stacks.apply_moves_crate_by_crate();

        assert_eq!(stacks.crates_on_top_as_string(), "CMZ");
    }

    #[test]
    fn move_crate_chunks_crates_on_top() {
        let mut stacks = parse_input(TEST_INPUT);

        stacks.apply_moves_crate_chunks();

        assert_eq!(stacks.crates_on_top_as_string(), "MCD");
    }

    #[test]
    fn problem1() {
        let mut stacks = parse_input(INPUT);

        stacks.apply_moves_crate_by_crate();

        assert_eq!(stacks.crates_on_top_as_string(), "JRVNHHCSJ");
    }

    #[test]
    fn problem2() {
        let mut stacks = parse_input(INPUT);

        stacks.apply_moves_crate_chunks();

        assert_eq!(stacks.crates_on_top_as_string(), "GNFBSBJLH");
    }
}
