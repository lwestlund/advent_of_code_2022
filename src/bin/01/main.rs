#![feature(test)]

extern crate test;

use std::fs;

const INPUT_PATH: &str = "src/bin/01/input.txt";
const TOP_N_ELVES: usize = 3;

type CalorieType = u32;

fn main() {
    let calories_per_elf = parse_calories(INPUT_PATH);

    let max_calories = max_elf_calories(&calories_per_elf);
    println!("Calories carried by top elf: {max_calories}");

    let top_n_calorie_sum = top_n_elf_calorie_sum(calories_per_elf, TOP_N_ELVES);
    println!("Calories carried by top {TOP_N_ELVES} elves: {top_n_calorie_sum}");
}

fn parse_calories(path: &str) -> Vec<CalorieType> {
    fs::read_to_string(path)
        .expect("Failed to read file")
        .split("\n\n")
        .map(|elf_calories| {
            elf_calories
                .trim()
                .split('\n')
                .map(|calories| calories.parse::<CalorieType>().unwrap())
                .sum()
        })
        .collect()
}

fn max_elf_calories(calories_per_elf: &[CalorieType]) -> CalorieType {
    *calories_per_elf.iter().max().unwrap()
}

fn top_n_elf_calorie_sum(mut calories_per_elf: Vec<CalorieType>, n: usize) -> CalorieType {
    calories_per_elf.sort_by(|a, b| b.cmp(a));
    calories_per_elf[..n].iter().sum::<CalorieType>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn problem1() {
        let calories_per_elf = parse_calories(INPUT_PATH);
        let max_elf_calories = max_elf_calories(&calories_per_elf);
        assert_eq!(max_elf_calories, 66306);
    }

    #[test]
    fn problem2() {
        let calories_per_elf = parse_calories(INPUT_PATH);
        let top_n_calorie_sum = top_n_elf_calorie_sum(calories_per_elf, TOP_N_ELVES);
        assert_eq!(top_n_calorie_sum, 195292);
    }
}

#[cfg(test)]
mod bench_01 {
    use std::process::Termination;
    use test::Bencher;

    use super::*;

    #[bench]
    fn bench_main(b: &mut Bencher) -> impl Termination {
        b.iter(|| main());
    }
}
