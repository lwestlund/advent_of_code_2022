use std::collections::VecDeque;

const CRT_LINE_WIDTH: u32 = 40;

pub struct Cpu {
    reg_x: i32,
    queued_instructions: VecDeque<Instruction>,
    current_instruction: Instruction,
    remaining_instruction_cycles: u32,
    num_completed_cycles: u32,
    pixel_buffer: [char; 240],
}
impl Cpu {
    pub fn new() -> Self {
        Self {
            reg_x: 1,
            queued_instructions: VecDeque::new(),
            current_instruction: Instruction::Noop,
            remaining_instruction_cycles: 1,
            num_completed_cycles: 0,
            pixel_buffer: ['.'; 240],
        }
    }

    /// Flushes the current instruction queue and loads the given instructions for execution.
    pub fn load_instructions(&mut self, instructions: Vec<Instruction>) {
        self.num_completed_cycles = 0;
        self.queued_instructions = instructions.into();
        self.fetch_next_instruction();
    }

    pub fn start_cycle(&mut self) {
        let pixel_line_position = self.num_completed_cycles.rem_euclid(CRT_LINE_WIDTH);
        if (self.reg_x - 1 <= pixel_line_position as i32)
            && (pixel_line_position as i32 <= self.reg_x + 1)
        {
            self.pixel_buffer[self.num_completed_cycles as usize] = '#';
        }
    }

    pub fn finalize_cycle(&mut self) {
        self.num_completed_cycles += 1;

        self.remaining_instruction_cycles -= 1;

        match self.current_instruction {
            Instruction::Noop => (),
            Instruction::AddX(v) => {
                if self.remaining_instruction_cycles == 0 {
                    self.reg_x += v;
                }
            }
        }

        if self.remaining_instruction_cycles == 0 {
            self.fetch_next_instruction();
        }
    }

    pub fn tick_to_cycle_start(&mut self, cycle: u32) {
        for _ in self.num_completed_cycles..cycle - 1 {
            self.start_cycle();
            self.finalize_cycle();
        }
        self.start_cycle();
    }

    pub fn signal_strength(&self) -> i32 {
        (self.num_completed_cycles + 1) as i32 * self.reg_x
    }

    fn fetch_next_instruction(&mut self) {
        let instruction = self.next_instruction();
        match instruction {
            Instruction::Noop => self.remaining_instruction_cycles = 1,
            Instruction::AddX(_) => self.remaining_instruction_cycles = 2,
        }
        self.current_instruction = instruction;
    }

    fn next_instruction(&mut self) -> Instruction {
        self.queued_instructions
            .pop_front()
            .unwrap_or(Instruction::Noop)
    }

    pub fn get_pixel_buffer_as_string(&self) -> String {
        self.pixel_buffer
            .chunks(40)
            .map(|pixel_row| pixel_row.iter().collect::<String>())
            .collect::<Vec<_>>()
            .join("\n")
    }
}

#[derive(Debug, PartialEq)]
pub enum Instruction {
    Noop,
    AddX(i32),
}
impl Instruction {
    pub fn from_str(s: &str) -> Self {
        let parts = s.split_whitespace().collect::<Vec<_>>();
        match parts[0] {
            "noop" => Self::Noop,
            "addx" => Self::AddX(parts[1].parse().unwrap()),
            _ => panic!("Unknown instruction"),
        }
    }
}

#[cfg(test)]
mod cpu_tests {
    use crate::{
        input::{TEST_INPUT_1, TEST_INPUT_2},
        parser::parse_input,
    };

    use super::*;

    #[test]
    fn test_load_instructions() {
        use Instruction::*;
        let instructions = vec![Noop, AddX(3)];
        let mut cpu = Cpu::new();
        cpu.load_instructions(instructions);

        assert_eq!(cpu.current_instruction, Noop);
        assert_eq!(cpu.queued_instructions, vec![AddX(3)]);
    }

    #[test]
    fn test_run_cycles() {
        use Instruction::*;
        let instructions = vec![Noop, AddX(3)];
        let mut cpu = Cpu::new();
        cpu.load_instructions(instructions);

        assert_eq!(cpu.num_completed_cycles, 0);

        cpu.start_cycle();
        assert_eq!(cpu.current_instruction, Noop);
        assert_eq!(cpu.remaining_instruction_cycles, 1);
        assert_eq!(cpu.reg_x, 1);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 1);

        cpu.start_cycle();
        assert_eq!(cpu.current_instruction, AddX(3));
        assert_eq!(cpu.remaining_instruction_cycles, 2);
        assert_eq!(cpu.reg_x, 1);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 2);

        cpu.start_cycle();
        assert_eq!(cpu.current_instruction, AddX(3));
        assert_eq!(cpu.remaining_instruction_cycles, 1);
        assert_eq!(cpu.reg_x, 1);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 3);

        cpu.start_cycle();
        assert_eq!(cpu.current_instruction, Noop);
        assert_eq!(cpu.remaining_instruction_cycles, 1);
        assert_eq!(cpu.reg_x, 1 + 3);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 4);
    }

    #[test]
    fn test_input_1_ticks() {
        let instructions = parse_input(TEST_INPUT_1);
        let mut cpu = Cpu::new();
        cpu.load_instructions(instructions);

        assert_eq!(cpu.num_completed_cycles, 0);
        assert_eq!(cpu.reg_x, 1);

        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 1);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 1);

        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 1);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 2);

        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 1);
        cpu.finalize_cycle();
        assert_eq!(cpu.reg_x, 4);
        assert_eq!(cpu.num_completed_cycles, 3);

        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 4);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 4);

        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 4);
        cpu.finalize_cycle();
        assert_eq!(cpu.reg_x, -1);
        assert_eq!(cpu.num_completed_cycles, 5);
    }

    #[test]
    fn test_input_2_ticks() {
        let instructions = parse_input(TEST_INPUT_2);
        let mut cpu = Cpu::new();
        cpu.load_instructions(instructions);

        cpu.tick_to_cycle_start(20);
        assert_eq!(cpu.num_completed_cycles, 19);
        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 21);
        assert_eq!(cpu.signal_strength(), 20 * 21);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 20);

        cpu.tick_to_cycle_start(60);
        assert_eq!(cpu.num_completed_cycles, 59);
        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 19);
        assert_eq!(cpu.signal_strength(), 60 * 19);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 60);

        cpu.tick_to_cycle_start(100);
        assert_eq!(cpu.num_completed_cycles, 99);
        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 18);
        assert_eq!(cpu.signal_strength(), 100 * 18);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 100);

        cpu.tick_to_cycle_start(140);
        assert_eq!(cpu.num_completed_cycles, 139);
        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 21);
        assert_eq!(cpu.signal_strength(), 140 * 21);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 140);

        cpu.tick_to_cycle_start(180);
        assert_eq!(cpu.num_completed_cycles, 179);
        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 16);
        assert_eq!(cpu.signal_strength(), 180 * 16);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 180);

        cpu.tick_to_cycle_start(220);
        assert_eq!(cpu.num_completed_cycles, 219);
        cpu.start_cycle();
        assert_eq!(cpu.reg_x, 18);
        assert_eq!(cpu.signal_strength(), 220 * 18);
        cpu.finalize_cycle();
        assert_eq!(cpu.num_completed_cycles, 220);
    }

    #[test]
    fn test_get_pixel_buffer_as_string() {
        let instructions = parse_input(TEST_INPUT_2);
        let mut cpu = Cpu::new();
        cpu.load_instructions(instructions);
        cpu.tick_to_cycle_start(240);
        cpu.finalize_cycle();

        let expected_pixel_buffer = "##..##..##..##..##..##..##..##..##..##..\n\
                                     ###...###...###...###...###...###...###.\n\
                                     ####....####....####....####....####....\n\
                                     #####.....#####.....#####.....#####.....\n\
                                     ######......######......######......####\n\
                                     #######.......#######.......#######.....";
        assert_eq!(cpu.get_pixel_buffer_as_string(), expected_pixel_buffer);
    }
}

#[cfg(test)]
mod instruction_tests {
    use super::*;

    #[test]
    fn test_instruction_from_str() {
        use Instruction::*;

        let instruction = Instruction::from_str("noop");
        assert_eq!(instruction, Noop);

        let instruction = Instruction::from_str("addx 3");
        assert_eq!(instruction, AddX(3));

        let instruction = Instruction::from_str("addx -5");
        assert_eq!(instruction, AddX(-5));
    }
}
