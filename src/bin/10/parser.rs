use std::fs;

use crate::cpu::Instruction;

pub fn parse_input(path: &str) -> Vec<Instruction> {
    fs::read_to_string(path)
        .expect("Failed to read file")
        .lines()
        .map(Instruction::from_str)
        .collect()
}

#[cfg(test)]
mod tests {
    use crate::input::{TEST_INPUT_1, TEST_INPUT_2};

    use super::*;

    #[test]
    fn test_parse_input_1() {
        let instructions = parse_input(TEST_INPUT_1);

        use Instruction::*;
        let expected = vec![Noop, AddX(3), AddX(-5)];
        assert_eq!(
            instructions
                .into_iter()
                .take(expected.len())
                .collect::<Vec<_>>(),
            expected
        );
    }

    #[test]
    fn test_parse_input_2() {
        let instructions = parse_input(TEST_INPUT_2);

        use Instruction::*;
        let expected = vec![
            AddX(15),
            AddX(-11),
            AddX(6),
            AddX(-3),
            AddX(5),
            AddX(-1),
            AddX(-8),
            AddX(13),
            AddX(4),
            Noop,
            AddX(-1),
        ];
        assert_eq!(
            instructions
                .into_iter()
                .take(expected.len())
                .collect::<Vec<_>>(),
            expected
        );
    }
}
