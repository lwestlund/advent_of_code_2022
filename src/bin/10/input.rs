pub const INPUT: &str = "src/bin/10/input.txt";
#[cfg(test)]
pub const TEST_INPUT_1: &str = "src/bin/10/test_input_1.txt";
#[cfg(test)]
pub const TEST_INPUT_2: &str = "src/bin/10/test_input_2.txt";
