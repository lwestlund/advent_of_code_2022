mod cpu;
mod input;
mod parser;

use cpu::Cpu;
use input::INPUT;
use parser::parse_input;

fn main() {
    let instructions = parse_input(INPUT);
    let mut cpu = Cpu::new();
    cpu.load_instructions(instructions);

    let p1_signal_strength_sum = problem1(&mut cpu);
    println!("Problem 1 signal strength sum: {p1_signal_strength_sum}");

    cpu.tick_to_cycle_start(240);
    cpu.finalize_cycle();
    let pixel_buffer_string = cpu.get_pixel_buffer_as_string();
    println!("Problem 2 pixel buffer:\n{pixel_buffer_string}");
}

fn problem1(cpu: &mut Cpu) -> i32 {
    let cycle_counts = vec![20, 60, 100, 140, 180, 220];

    cycle_counts
        .iter()
        .map(|cycle_count| {
            cpu.tick_to_cycle_start(*cycle_count);
            let signal_strength = cpu.signal_strength();
            cpu.finalize_cycle();
            signal_strength
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use crate::input::TEST_INPUT_2;

    use super::*;

    #[test]
    fn test_problem1() {
        let instructions = parse_input(TEST_INPUT_2);
        let mut cpu = Cpu::new();
        cpu.load_instructions(instructions);
        assert_eq!(problem1(&mut cpu), 13140);
    }
}
