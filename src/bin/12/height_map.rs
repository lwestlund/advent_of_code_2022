use pathfinding::prelude::astar;
use std::fmt::Display;

pub struct HeightMap {
    map: Vec<Vec<HeightType>>,
    width: usize,
    height: usize,
    start_position: Position,
    end_position: Position,
}

type HeightType = u8;
impl HeightMap {
    pub fn from_str(s: &str) -> Self {
        let mut start_position = Position { x: 0, y: 0 };
        let mut end_position = Position { x: 0, y: 0 };
        let map = s
            .lines()
            .enumerate()
            .map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .map(|(x, c)| {
                        let c = if c == 'S' {
                            start_position.x = x;
                            start_position.y = y;
                            'a'
                        } else if c == 'E' {
                            end_position.x = x;
                            end_position.y = y;
                            'z'
                        } else {
                            c
                        };
                        c as HeightType - b'a'
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        let height = map.len();
        let width = map[0].len();
        Self {
            map,
            width,
            height,
            start_position,
            end_position,
        }
    }

    pub fn get_shortest_path_s_to_e(&self) -> Option<(Vec<Position>, u32)> {
        let start = &self.start_position;
        let end = &self.end_position;
        astar(
            start,
            |p| p.successors(self),
            |p| p.distance(end),
            |p| p == end,
        )
    }

    pub fn get_shortest_path_a_to_e(&self) -> Option<(Vec<Position>, u32)> {
        let all_a: Vec<Position> = self
            .map
            .iter()
            .enumerate()
            .flat_map(|(y, row)| {
                row.iter()
                    .enumerate()
                    .filter_map(|(x, height)| {
                        if *height == 0 {
                            Some(Position { x, y })
                        } else {
                            None
                        }
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        let mut shortest_path = (vec![], u32::MAX);
        for a in &all_a {
            let shortest_path_from_a = astar(
                a,
                |p| p.successors(self),
                |p| p.distance(&self.end_position),
                |p| *p == self.end_position,
            );
            if let Some(shortest_path_from_a) = shortest_path_from_a {
                if shortest_path_from_a.1 < shortest_path.1 {
                    shortest_path = shortest_path_from_a;
                }
            } else {
                continue;
            }
        }
        if shortest_path.1 < u32::MAX {
            Some(shortest_path)
        } else {
            None
        }
    }
}

impl Display for HeightMap {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.map
                .iter()
                .map(|chars| chars.iter().map(|u| (u + b'a') as char).collect::<String>())
                .collect::<Vec<_>>()
                .join("\n")
        )
    }
}

#[derive(PartialEq, Eq, Hash, PartialOrd, Ord, Clone)]
pub struct Position {
    x: usize,
    y: usize,
}
impl Position {
    fn distance(&self, other: &Self) -> u32 {
        (self.x.abs_diff(other.x) + self.y.abs_diff(other.y)) as u32
    }

    fn successors(&self, height_map: &HeightMap) -> Vec<(Position, u32)> {
        let &Position { x, y } = self;
        let mut successors = Vec::new();
        let height_plus_one = height_map.map[y][x] + 1;
        if x > 0 && height_map.map[y][x - 1] <= height_plus_one {
            successors.push((Position { x: x - 1, y }, 1));
        }
        if x < height_map.width - 1 && height_map.map[y][x + 1] <= height_plus_one {
            successors.push((Position { x: x + 1, y }, 1));
        }
        if y > 0 && height_map.map[y - 1][x] <= height_plus_one {
            successors.push((Position { x, y: y - 1 }, 1));
        }
        if y < height_map.height - 1 && height_map.map[y + 1][x] <= height_plus_one {
            successors.push((Position { x, y: y + 1 }, 1));
        }
        successors
    }
}
