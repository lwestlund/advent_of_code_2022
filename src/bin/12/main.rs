use std::fs;

mod height_map;

use height_map::HeightMap;

const INPUT: &str = "src/bin/12/input.txt";

fn main() {
    let map = parse_input(INPUT);

    // println!("map:\n{map}");
    let shortest_path = map.get_shortest_path_s_to_e().expect("No path found");
    println!("Shortest path length S to E: {}", shortest_path.1);

    let real_shortest_path = map.get_shortest_path_a_to_e().expect("No path found");
    println!(
        "Shortest path length any 'a' to E: {}",
        real_shortest_path.1
    );
}

fn parse_input(path: &str) -> HeightMap {
    HeightMap::from_str(&fs::read_to_string(path).expect("Failed to read file"))
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "src/bin/12/test_input.txt";

    #[test]
    fn test_input_problem1() {
        let map = parse_input(TEST_INPUT);
        let shortest_path = map.get_shortest_path_s_to_e().expect("No path found");
        assert_eq!(shortest_path.1, 31);
    }

    #[test]
    fn test_input_problem2() {
        let map = parse_input(TEST_INPUT);
        let shortest_path = map.get_shortest_path_a_to_e().expect("No path found");
        assert_eq!(shortest_path.1, 29);
    }

    #[test]
    fn test_problem1() {
        let map = parse_input(INPUT);
        let shortest_path = map.get_shortest_path_s_to_e().expect("No path found");
        assert_eq!(shortest_path.1, 534);
    }

    #[test]
    fn test_problem2() {
        let map = parse_input(INPUT);
        let shortest_path = map.get_shortest_path_a_to_e().expect("No path found");
        assert_eq!(shortest_path.1, 525);
    }
}
