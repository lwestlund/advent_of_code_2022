use std::{collections::BTreeMap, fs, path::PathBuf};

type DirectorySizeMap = BTreeMap<PathBuf, u64>;

const INPUT: &str = "src/bin/07/input.txt";

const PROBLEM_1_MAX_DIR_SIZE: u64 = 100_000;

const TOTAL_DISK_SPACE: u64 = 70_000_000;
const REQUIRED_FREE_SPACE_FOR_UPDATE: u64 = 30_000_000;

fn main() {
    let input = parse_input(INPUT);
    let directory_size_map = get_directory_sizes(&input);

    let problem_one_solution =
        sum_of_dir_size_less_than(&directory_size_map, PROBLEM_1_MAX_DIR_SIZE);
    println!("Sum of directory sizes with size less than {PROBLEM_1_MAX_DIR_SIZE}: {problem_one_solution}");

    let (dir, size) =
        smallest_dir_to_make_free_space(&directory_size_map, REQUIRED_FREE_SPACE_FOR_UPDATE);
    println!("Smallest size directory that would free enough space for the update: {dir:?} with size {size}");
}

fn parse_input(path: &str) -> String {
    fs::read_to_string(path).expect("Failed to read file")
}

fn get_directory_sizes(s: &str) -> DirectorySizeMap {
    let mut directories: DirectorySizeMap = BTreeMap::from([("/".into(), 0)]);
    let mut files: BTreeMap<PathBuf, u64> = BTreeMap::new();
    let root: PathBuf = ["/"].iter().collect();
    let mut cwd = root.clone();
    for line in s.lines() {
        if line.starts_with('$') {
            let command_line = line.split_whitespace().skip(1).collect::<Vec<_>>();
            let command = command_line[0];
            if command == "cd" {
                let arg = command_line[1];
                if arg == ".." {
                    cwd.pop();
                } else if arg == "/" {
                    cwd = root.clone();
                } else {
                    cwd.push(arg);
                }
            } else if command == "ls" {
                // Nothing to do.
            }
        } else if line.starts_with("dir ") {
            let dir = line.split_whitespace().collect::<Vec<_>>()[1];
            let mut path = cwd.clone();
            path.push(dir);
            directories.insert(path, 0);
        } else {
            // If it is not a command or a listing of a directory, it is a file.
            let parts = line.split_whitespace().collect::<Vec<_>>();
            let size = parts[0].parse().unwrap();
            let name = parts[1].to_owned();
            let mut path = cwd.clone();
            path.push(name);
            files.insert(path, size);
        }
    }

    directories.iter_mut().for_each(|dir| {
        let total_file_size = files
            .iter()
            .filter(|file| file.0.starts_with(dir.0))
            .map(|file| file.1)
            .sum::<u64>();
        *dir.1 = total_file_size;
    });

    directories
}

fn sum_of_dir_size_less_than(directory_size_map: &DirectorySizeMap, n: u64) -> u64 {
    directory_size_map.values().filter(|v| **v < n).sum()
}

fn smallest_dir_to_make_free_space(
    directory_size_map: &DirectorySizeMap,
    required_free_space: u64,
) -> (&PathBuf, &u64) {
    let root: PathBuf = ["/"].iter().collect();
    let used_space = directory_size_map.get(&root).unwrap();
    let free_space = TOTAL_DISK_SPACE - used_space;
    let space_to_clean = required_free_space - free_space;
    directory_size_map
        .iter()
        .filter(|(_dir, size)| **size >= space_to_clean)
        // Find the directory-size-pair with smallest size.
        .reduce(|(d1, s1), (d2, s2)| if s1 < s2 { (d1, s1) } else { (d2, s2) })
        .unwrap()
}

#[cfg(test)]
mod problem1 {
    use super::*;

    const TEST_INPUT: &str = "src/bin/07/test_input.txt";

    #[test]
    fn test_data_problem1() {
        let input = parse_input(TEST_INPUT);
        let directory_size_map = get_directory_sizes(&input);
        assert_eq!(
            sum_of_dir_size_less_than(&directory_size_map, PROBLEM_1_MAX_DIR_SIZE),
            95437
        );
    }

    #[test]
    fn test_problem1() {
        let input = parse_input(INPUT);
        let directory_size_map = get_directory_sizes(&input);
        assert_eq!(
            sum_of_dir_size_less_than(&directory_size_map, PROBLEM_1_MAX_DIR_SIZE),
            1783610
        );
    }

    #[test]
    fn test_data_problem2() {
        let input = parse_input(TEST_INPUT);
        let directory_size_map = get_directory_sizes(&input);
        assert_eq!(
            smallest_dir_to_make_free_space(&directory_size_map, REQUIRED_FREE_SPACE_FOR_UPDATE),
            (&["/", "d"].iter().collect::<PathBuf>(), &24933642)
        );
    }

    #[test]
    fn test_problem2() {
        let input = parse_input(INPUT);
        let directory_size_map = get_directory_sizes(&input);
        assert_eq!(
            smallest_dir_to_make_free_space(&directory_size_map, REQUIRED_FREE_SPACE_FOR_UPDATE),
            (
                &["/", "nfn", "qmrsvfvw", "fpljqj", "tdnp"]
                    .iter()
                    .collect::<PathBuf>(),
                &4370655
            )
        );
    }
}
