mod subroutine;

use std::fs;

use subroutine::offset_until_n_unique;

const INPUT: &str = "src/bin/06/input.txt";

fn main() {
    let data_buffer = parse_input(INPUT);

    if let Some(start_of_packet_offset) = offset_until_n_unique(&data_buffer, 4) {
        println!("Start-of-packet offset: {start_of_packet_offset}");
    } else {
        println!("No start-of-packet marker found");
    }

    if let Some(start_of_message_offset) = offset_until_n_unique(&data_buffer, 14) {
        println!("Start-of-message offset: {start_of_message_offset}");
    } else {
        println!("No start-of-message marker found");
    }
}

fn parse_input(path: &str) -> String {
    fs::read_to_string(path).expect("Failed to read file")
}

#[cfg(test)]
mod day06 {
    use super::*;

    #[test]
    fn problem1() {
        let data_buffer = parse_input(INPUT);

        let start_of_packet_marker_length = 4;
        let packet_start_offset =
            offset_until_n_unique(&data_buffer, start_of_packet_marker_length);
        assert!(packet_start_offset.is_some());
        assert_eq!(packet_start_offset.unwrap(), 1578);
    }

    #[test]
    fn problem2() {
        let data_buffer = parse_input(INPUT);

        let start_of_packet_marker_length = 14;
        let message_start_offset =
            offset_until_n_unique(&data_buffer, start_of_packet_marker_length);
        assert!(message_start_offset.is_some());
        assert_eq!(message_start_offset.unwrap(), 2178);
    }
}
