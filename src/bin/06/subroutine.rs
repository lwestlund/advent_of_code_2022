pub fn offset_until_n_unique(s: &str, n: usize) -> Option<usize> {
    let marker_length_minus_one = n - 1;
    for i in 0..(s.len() - marker_length_minus_one) {
        let vec = s[i..(i + n)].chars().collect::<Vec<_>>();
        let mut no_duplicate = true;
        for (idx, elem) in vec.iter().take(n - 1).enumerate() {
            let left_of_elem = &vec[..idx];
            let right_of_elem = &vec[idx + 1..];
            if left_of_elem.contains(elem) || right_of_elem.contains(elem) {
                no_duplicate = false;
                break;
            }
        }
        if no_duplicate {
            return Some(i + n);
        }
    }
    None
}

#[cfg(test)]
mod subroutine {
    use super::*;

    #[test]
    fn num_chars_until_packet_start() {
        let start_of_package_marker_length = 4;
        assert_eq!(
            offset_until_n_unique(
                "mjqjpqmgbljsphdztnvjfqwrcgsmlb",
                start_of_package_marker_length
            ),
            Some(7)
        );
        assert_eq!(
            offset_until_n_unique(
                "bvwbjplbgvbhsrlpgdmjqwftvncz",
                start_of_package_marker_length
            ),
            Some(5)
        );
        assert_eq!(
            offset_until_n_unique(
                "nppdvjthqldpwncqszvftbrmjlhg",
                start_of_package_marker_length
            ),
            Some(6)
        );
        assert_eq!(
            offset_until_n_unique(
                "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg",
                start_of_package_marker_length
            ),
            Some(10)
        );
        assert_eq!(
            offset_until_n_unique(
                "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw",
                start_of_package_marker_length
            ),
            Some(11)
        );
    }

    #[test]
    fn num_chars_until_message_start() {
        let start_of_message_marker_length = 14;
        assert_eq!(
            offset_until_n_unique(
                "mjqjpqmgbljsphdztnvjfqwrcgsmlb",
                start_of_message_marker_length
            ),
            Some(19)
        );
        assert_eq!(
            offset_until_n_unique(
                "bvwbjplbgvbhsrlpgdmjqwftvncz",
                start_of_message_marker_length
            ),
            Some(23)
        );
        assert_eq!(
            offset_until_n_unique(
                "nppdvjthqldpwncqszvftbrmjlhg",
                start_of_message_marker_length
            ),
            Some(23)
        );
        assert_eq!(
            offset_until_n_unique(
                "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg",
                start_of_message_marker_length
            ),
            Some(29)
        );
        assert_eq!(
            offset_until_n_unique(
                "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw",
                start_of_message_marker_length
            ),
            Some(26)
        );
    }
}
