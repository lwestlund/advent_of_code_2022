pub fn item_priority(item: char) -> u32 {
    if item.is_lowercase() {
        item as u32 - 'a' as u32 + 1
    } else {
        item as u32 - 'A' as u32 + 27
    }
}

#[derive(Debug)]
pub struct Rucksack(Vec<u8>);

impl Rucksack {
    pub fn new(contents: &str) -> Self {
        Self(contents.as_bytes().to_vec())
    }

    fn compartment_one(&self) -> &[u8] {
        &self.0[..self.0.len() / 2]
    }

    fn compartment_two(&self) -> &[u8] {
        &self.0[self.0.len() / 2..]
    }

    pub fn misplaced_item_types(&self) -> Vec<char> {
        let mut misplaced_item_types = Vec::new();
        for item_c1 in self.compartment_one() {
            for item_c2 in self.compartment_two() {
                if item_c1 == item_c2 {
                    let item = *item_c1 as char;
                    if !misplaced_item_types.contains(&item) {
                        misplaced_item_types.push(item);
                        break;
                    }
                }
            }
        }
        misplaced_item_types
    }

    pub fn content_overlap(&self, other: &Self) -> Self {
        let mut overlap = self
            .0
            .iter()
            .filter_map(|item| {
                if other.0.contains(item) {
                    Some(*item)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        overlap.sort();
        overlap.dedup();
        Self(overlap)
    }

    pub fn prio_sum(&self) -> u32 {
        self.0.iter().map(|&item| item_priority(item as char)).sum()
    }
}

#[cfg(test)]
mod rucksack_tests {
    use super::*;

    #[test]
    fn compartments() {
        let rucksack = Rucksack::new("abcd");

        assert_eq!(rucksack.compartment_one(), &['a' as u8, 'b' as u8]);
        assert_eq!(rucksack.compartment_two(), &['c' as u8, 'd' as u8]);
    }

    #[test]
    fn misplaced_item_types() {
        let rucksack = Rucksack::new("abcaxc");

        let misplaced_item_types = rucksack.misplaced_item_types();

        assert_eq!(misplaced_item_types, vec!['a', 'c']);

        let rucksack = Rucksack::new("aaaaaa");

        let misplaced_item_types = rucksack.misplaced_item_types();

        assert_eq!(misplaced_item_types, vec!['a']);
    }

    #[test]
    fn item_prio() {
        assert_eq!(item_priority('a'), 1);
        assert_eq!(item_priority('b'), 2);
        assert_eq!(item_priority('z'), 26);
        assert_eq!(item_priority('A'), 27);
        assert_eq!(item_priority('Y'), 51);
        assert_eq!(item_priority('Z'), 52);
    }
}
