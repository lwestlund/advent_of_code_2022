use std::fs;

mod rucksack;

use rucksack::{item_priority, Rucksack};

const INPUT: &str = "src/bin/03/input.txt";

fn main() {
    let rucksacks = read_input(INPUT);

    let total_prio = misplaced_items_prio_sum(&rucksacks);
    println!("Total prio of misplaced items: {total_prio}");

    let total_badge_prio = group_bade_prio_sum(&rucksacks);
    println!("Total badge prio: {total_badge_prio}");
}

fn read_input(path: &str) -> Vec<Rucksack> {
    fs::read_to_string(path)
        .expect("Failed to read file")
        .lines()
        .map(Rucksack::new)
        .collect()
}

/// Problem 1
fn misplaced_items_prio_sum(rucksacks: &[Rucksack]) -> u32 {
    rucksacks
        .iter()
        .map(|r| {
            r.misplaced_item_types()
                .iter()
                .map(|item| item_priority(*item))
                .sum::<u32>()
        })
        .sum()
}

/// Problem 2
fn group_bade_prio_sum(rucksacks: &[Rucksack]) -> u32 {
    let elves_per_group = 3;
    rucksacks
        .chunks(elves_per_group)
        .map(|group| {
            let overlap = group[0].content_overlap(&group[1]);
            let overlap = overlap.content_overlap(&group[2]);
            overlap.prio_sum()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "src/bin/03/test_input.txt";

    #[test]
    fn test_data_misplaced_item_types() {
        let rucksacks = read_input(TEST_INPUT);

        assert_eq!(rucksacks.len(), 6);
        assert_eq!(rucksacks[0].misplaced_item_types(), vec!['p']);
        assert_eq!(rucksacks[1].misplaced_item_types(), vec!['L']);
        assert_eq!(rucksacks[2].misplaced_item_types(), vec!['P']);
        assert_eq!(rucksacks[3].misplaced_item_types(), vec!['v']);
        assert_eq!(rucksacks[4].misplaced_item_types(), vec!['t']);
        assert_eq!(rucksacks[5].misplaced_item_types(), vec!['s']);
    }

    #[test]
    fn test_data_total_rucksack_prio() {
        let rucksacks = read_input(TEST_INPUT);

        assert_eq!(misplaced_items_prio_sum(&rucksacks), 157);
    }

    #[test]
    fn problem1() {
        let rucksacks = read_input(INPUT);

        assert_eq!(misplaced_items_prio_sum(&rucksacks), 7872);
    }

    #[test]
    fn test_data_badge_prio_sum() {
        let rucksacks = read_input(TEST_INPUT);

        assert_eq!(group_bade_prio_sum(&rucksacks), 70);
    }

    #[test]
    fn problem2() {
        let rucksacks = read_input(INPUT);

        assert_eq!(group_bade_prio_sum(&rucksacks), 2497);
    }
}
