use std::fs;

use work_pair::WorkPair;

mod sections;
mod work_pair;

const INPUT: &str = "src/bin/04/input.txt";

fn main() {
    let work_pairs = parse_input(INPUT);

    let num_fully_contained_work_pairs = count_fully_contained_work_pairs(&work_pairs);
    println!("Number of fully contained pairs: {num_fully_contained_work_pairs}");

    let num_overlapping_work_pairs = count_overlapping_work_pairs(&work_pairs);
    println!("Number of overlapping work pairs: {num_overlapping_work_pairs}");
}

fn parse_input(path: &str) -> Vec<WorkPair> {
    fs::read_to_string(path)
        .expect("Failed to read file")
        .lines()
        .map(WorkPair::from_str)
        .collect()
}

fn count_fully_contained_work_pairs(work_pairs: &[WorkPair]) -> usize {
    work_pairs
        .iter()
        .filter_map(|work_pair| work_pair.has_complete_sections_overlap().then_some(()))
        .count()
}

fn count_overlapping_work_pairs(work_pairs: &[WorkPair]) -> usize {
    work_pairs
        .iter()
        .filter_map(|work_pair| work_pair.has_overlapping_sections().then_some(()))
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "src/bin/04/test_input.txt";

    #[test]
    fn test_data_count_fully_contained_work_pairs() {
        let work_pairs = parse_input(TEST_INPUT);

        assert_eq!(count_fully_contained_work_pairs(&work_pairs), 2);
    }

    #[test]
    fn test_data_count_overlapping_pairs() {
        let work_pairs = parse_input(TEST_INPUT);

        assert_eq!(count_overlapping_work_pairs(&work_pairs), 4);
    }

    #[test]
    fn test_count_fully_contained_work_pairs() {
        let work_pairs = parse_input(INPUT);

        assert_eq!(count_fully_contained_work_pairs(&work_pairs), 528);
    }

    #[test]
    fn test_count_overlapping_work_pairs() {
        let work_pairs = parse_input(INPUT);

        assert_eq!(count_overlapping_work_pairs(&work_pairs), 881);
    }
}
