use crate::sections::Sections;

pub struct WorkPair(Vec<Sections>);

impl WorkPair {
    pub fn from_str(s: &str) -> Self {
        Self(s.split(',').take(2).map(Sections::from_str).collect())
    }

    pub fn has_complete_sections_overlap(&self) -> bool {
        let first = &self.0[0];
        let second = &self.0[1];
        first.contains(second) || second.contains(first)
    }

    pub fn has_overlapping_sections(&self) -> bool {
        let first = &self.0[0];
        let second = &self.0[1];
        first.overlaps(second) || second.overlaps(first)
    }
}

#[cfg(test)]
mod work_pair_tests {
    use super::*;

    #[test]
    fn test_overlap() {
        let work_pairs = WorkPair::from_str("1-4,5-6");
        assert!(!work_pairs.has_complete_sections_overlap());
        assert!(!work_pairs.has_overlapping_sections());
        let work_pairs = WorkPair::from_str("5-6,1-4");
        assert!(!work_pairs.has_complete_sections_overlap());
        assert!(!work_pairs.has_overlapping_sections());

        let work_pairs = WorkPair::from_str("1-4,2-5");
        assert!(!work_pairs.has_complete_sections_overlap());
        assert!(work_pairs.has_overlapping_sections());
        let work_pairs = WorkPair::from_str("2-5,1-4");
        assert!(!work_pairs.has_complete_sections_overlap());
        assert!(work_pairs.has_overlapping_sections());

        let work_pairs = WorkPair::from_str("1-4,2-3");
        assert!(work_pairs.has_complete_sections_overlap());
        assert!(work_pairs.has_overlapping_sections());
        let work_pairs = WorkPair::from_str("2-3,1-4");
        assert!(work_pairs.has_complete_sections_overlap());
        assert!(work_pairs.has_overlapping_sections());

        let work_pairs = WorkPair::from_str("1-4,0-3");
        assert!(!work_pairs.has_complete_sections_overlap());
        assert!(work_pairs.has_overlapping_sections());
        let work_pairs = WorkPair::from_str("0-3,1-4");
        assert!(!work_pairs.has_complete_sections_overlap());
        assert!(work_pairs.has_overlapping_sections());
    }
}
