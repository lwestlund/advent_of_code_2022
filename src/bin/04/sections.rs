pub struct Sections {
    start: u32,
    end: u32,
}

impl Sections {
    pub fn from_str(s: &str) -> Self {
        let boundaries = s
            .split('-')
            .map(|s| s.parse().unwrap())
            .collect::<Vec<u32>>();
        let start = boundaries[0];
        let end = boundaries[1];
        Self { start, end }
    }

    pub fn contains(&self, other: &Self) -> bool {
        self.contains_section(other.start) && self.contains_section(other.end)
    }

    fn contains_section(&self, section: u32) -> bool {
        self.start <= section && section <= self.end
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        self.contains_section(other.start) || self.contains_section(other.end)
    }
}

#[cfg(test)]
mod sections {
    use super::*;

    #[test]
    fn contains_section() {
        let sections = Sections::from_str("11-73");

        assert!(!sections.contains_section(10));
        assert!(sections.contains_section(11));
        assert!(sections.contains_section(12));
        assert!(sections.contains_section(73));
        assert!(!sections.contains_section(74));
    }

    #[test]
    fn contains() {
        let sections1 = Sections::from_str("11-73");
        let sections2 = Sections::from_str("12-73");

        assert!(sections1.contains(&sections2));
    }

    #[test]
    fn overlaps() {
        let sections1 = Sections::from_str("11-73");
        let sections2 = Sections::from_str("12-74");
        let sections3 = Sections::from_str("10-11");

        assert!(sections1.overlaps(&sections2));
        assert!(sections2.overlaps(&sections1));

        assert!(sections1.overlaps(&sections3));
        assert!(sections3.overlaps(&sections1));

        assert!(!sections2.overlaps(&sections3));
        assert!(!sections3.overlaps(&sections2));
    }
}
