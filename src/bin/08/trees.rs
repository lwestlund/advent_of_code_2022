pub struct Trees {
    trees: Vec<Vec<u8>>,
    num_rows: usize,
    num_cols: usize,
}

impl Trees {
    pub fn from_string(s: String) -> Self {
        let trees = s
            .lines()
            .map(|line| {
                line.chars()
                    .map(|c| c.to_digit(10).unwrap() as u8)
                    .collect::<Vec<u8>>()
            })
            .collect::<Vec<_>>();
        let num_rows = trees.len();
        let num_cols = trees[0].len();
        Self {
            trees,
            num_rows,
            num_cols,
        }
    }

    pub fn count_visible(&self) -> u32 {
        let row_indices = 0..self.num_rows;
        let col_indices = 0..self.num_cols;
        row_indices
            .map(|r| {
                col_indices
                    .clone()
                    .filter(|&c| self.visible_in_row(r, c) || self.visible_in_column(r, c))
                    .count() as u32
            })
            .sum()
    }

    fn visible_in_row(&self, row_idx: usize, col_idx: usize) -> bool {
        let tree = &self.trees[row_idx][col_idx];
        let west_of_tree = &self.trees[row_idx][..col_idx];
        let east_of_tree = &self.trees[row_idx][col_idx + 1..];
        west_of_tree.iter().all(|t| tree > t) || east_of_tree.iter().all(|t| tree > t)
    }

    fn visible_in_column(&self, row_idx: usize, col_idx: usize) -> bool {
        let tree = &self.trees[row_idx][col_idx];
        let north_of_tree = &self
            .trees
            .iter()
            .take(row_idx)
            .map(|row| row[col_idx])
            .collect::<Vec<_>>();
        let south_of_tree = &self
            .trees
            .iter()
            .skip(row_idx + 1)
            .map(|row| row[col_idx])
            .collect::<Vec<_>>();
        north_of_tree.iter().all(|t| tree > t) || south_of_tree.iter().all(|t| tree > t)
    }

    pub fn max_scenic_score(&self) -> usize {
        let row_indices = 0..self.num_rows;
        let col_indices = 0..self.num_cols;
        row_indices
            .map(|r| {
                col_indices
                    .clone()
                    .map(|c| self.scenic_score(r, c))
                    .max()
                    .unwrap()
            })
            .max()
            .unwrap()
    }

    fn num_visible_trees_west(&self, row_idx: usize, col_idx: usize) -> usize {
        let trees_west = &self.trees[row_idx][..col_idx];
        let tree = self.trees[row_idx][col_idx];
        Self::get_viewing_distance_in_direction(tree, trees_west.iter().rev())
    }
    fn num_visible_trees_east(&self, row_idx: usize, col_idx: usize) -> usize {
        let trees_east = &self.trees[row_idx][col_idx + 1..];
        let tree = self.trees[row_idx][col_idx];
        Self::get_viewing_distance_in_direction(tree, trees_east.iter())
    }
    fn num_visible_trees_north(&self, row_idx: usize, col_idx: usize) -> usize {
        let trees_north = (0..row_idx)
            .map(|r| self.trees[r][col_idx])
            .collect::<Vec<_>>();
        let tree = self.trees[row_idx][col_idx];
        Self::get_viewing_distance_in_direction(tree, trees_north.iter().rev())
    }
    fn num_visible_trees_south(&self, row_idx: usize, col_idx: usize) -> usize {
        let trees_south = (row_idx + 1..self.num_rows)
            .map(|r| self.trees[r][col_idx])
            .collect::<Vec<_>>();
        let tree = self.trees[row_idx][col_idx];
        Self::get_viewing_distance_in_direction(tree, trees_south.iter())
    }
    fn get_viewing_distance_in_direction<'a, I>(tree: u8, trees_in_direction: I) -> usize
    where
        I: Iterator<Item = &'a u8>,
    {
        let mut taller_tree_found = false;
        trees_in_direction
            .take_while(|t| {
                if taller_tree_found {
                    return false;
                }
                let can_see_over_tree = **t < tree;
                if !can_see_over_tree {
                    taller_tree_found = true;
                }
                can_see_over_tree || taller_tree_found
            })
            .count()
    }

    fn scenic_score(&self, row_idx: usize, col_idx: usize) -> usize {
        let num_visible_west = self.num_visible_trees_west(row_idx, col_idx);
        let num_visible_east = self.num_visible_trees_east(row_idx, col_idx);
        let num_visible_north = self.num_visible_trees_north(row_idx, col_idx);
        let num_visible_south = self.num_visible_trees_south(row_idx, col_idx);
        num_visible_west * num_visible_east * num_visible_north * num_visible_south
    }
}

#[cfg(test)]
mod trees {
    use super::*;

    #[test]
    fn trees_from_string() {
        let s = "0123\n\
                 4567\n\
                 89"
        .to_owned();
        let trees = Trees::from_string(s);
        assert_eq!(trees.trees[0], vec![0, 1, 2, 3]);
        assert_eq!(trees.trees[1], vec![4, 5, 6, 7]);
        assert_eq!(trees.trees[2], vec![8, 9]);
    }

    #[test]
    fn test_count_visible() {
        let s = "30373\n\
                 25512\n\
                 65332\n\
                 33549\n\
                 35390"
            .to_owned();
        let trees = Trees::from_string(s);
        assert_eq!(trees.count_visible(), 21);
    }

    #[test]
    fn test_num_visible_trees() {
        let s = "30373\n\
                 25512\n\
                 65332\n\
                 33549\n\
                 35390"
            .to_owned();
        let trees = Trees::from_string(s);

        assert_eq!(trees.num_visible_trees_north(1, 2), 1);
        assert_eq!(trees.num_visible_trees_west(1, 2), 1);
        assert_eq!(trees.num_visible_trees_south(1, 2), 2);
        assert_eq!(trees.num_visible_trees_east(1, 2), 2);

        assert_eq!(trees.num_visible_trees_north(3, 2), 2);
        assert_eq!(trees.num_visible_trees_west(3, 2), 2);
        assert_eq!(trees.num_visible_trees_south(3, 2), 1);
        assert_eq!(trees.num_visible_trees_east(3, 2), 2);
    }

    #[test]
    fn test_scenic_score() {
        let s = "30373\n\
                 25512\n\
                 65332\n\
                 33549\n\
                 35390"
            .to_owned();
        let trees = Trees::from_string(s);

        assert_eq!(trees.scenic_score(1, 2), 4);
        assert_eq!(trees.scenic_score(3, 2), 8);
    }

    #[test]
    fn test_max_scenic_score() {
        let s = "30373\n\
                 25512\n\
                 65332\n\
                 33549\n\
                 35390"
            .to_owned();
        let trees = Trees::from_string(s);

        assert_eq!(trees.max_scenic_score(), 8);
    }
}
