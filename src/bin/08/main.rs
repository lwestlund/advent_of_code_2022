use std::fs;

use trees::Trees;

mod trees;

const INPUT: &str = "src/bin/08/input.txt";

fn main() {
    let trees = parse_input(INPUT);

    let num_visible_trees = trees.count_visible();
    println!("The number of visible trees is: {num_visible_trees}");

    let max_scenic_score = trees.max_scenic_score();
    println!("The max scenic score is: {max_scenic_score}");
}

fn parse_input(path: &str) -> Trees {
    Trees::from_string(fs::read_to_string(path).expect("Failed to read file"))
}

#[cfg(test)]
mod day08 {
    use super::*;

    const TEST_INPUT: &str = "src/bin/08/test_input.txt";

    #[test]
    fn test_data_count_visible() {
        let trees = parse_input(TEST_INPUT);

        assert_eq!(trees.count_visible(), 21);
    }

    #[test]
    fn test_data_max_scenic_score() {
        let trees = parse_input(TEST_INPUT);

        assert_eq!(trees.max_scenic_score(), 8);
    }

    #[test]
    fn test_count_visible() {
        let trees = parse_input(INPUT);

        assert_eq!(trees.count_visible(), 1684);
    }

    #[test]
    fn test_max_scenic_score() {
        let trees = parse_input(INPUT);

        assert_eq!(trees.max_scenic_score(), 486540);
    }
}
