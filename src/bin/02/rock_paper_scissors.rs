pub type Score = u32;

#[derive(Debug, PartialEq)]
pub enum Outcome {
    Win,
    Loss,
    Draw,
}

impl Outcome {
    pub fn score(&self) -> Score {
        use Outcome::*;
        match self {
            Win => 6,
            Draw => 3,
            Loss => 0,
        }
    }

    pub fn decode(encoded_outcode: &str) -> Outcome {
        use Outcome::*;
        match encoded_outcode {
            "X" => Loss,
            "Y" => Draw,
            "Z" => Win,
            _ => unimplemented!("Unknown input"),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Hand {
    Rock,
    Paper,
    Scissors,
}

impl Hand {
    pub fn play_round(&self, rhs: Hand) -> Outcome {
        use Hand::*;
        use Outcome::*;
        match self {
            Rock => match rhs {
                Rock => Draw,
                Paper => Loss,
                Scissors => Win,
            },
            Paper => match rhs {
                Rock => Win,
                Paper => Draw,
                Scissors => Loss,
            },
            Scissors => match rhs {
                Rock => Loss,
                Paper => Win,
                Scissors => Draw,
            },
        }
    }

    pub fn score(&self) -> Score {
        use Hand::*;
        match self {
            Rock => 1,
            Paper => 2,
            Scissors => 3,
        }
    }

    pub fn decode(encoded_hand: &str) -> Hand {
        use Hand::*;
        match encoded_hand {
            "A" | "X" => Rock,
            "B" | "Y" => Paper,
            "C" | "Z" => Scissors,
            _ => unimplemented!("Unknown input"),
        }
    }

    pub fn from_opponent_hand_and_outcome(opponent_hand: Hand, outcome: Outcome) -> Hand {
        use Hand::*;
        use Outcome::*;
        match outcome {
            Win => match opponent_hand {
                Rock => Paper,
                Paper => Scissors,
                Scissors => Rock,
            },
            Draw => match opponent_hand {
                Rock => Rock,
                Paper => Paper,
                Scissors => Scissors,
            },
            Loss => match opponent_hand {
                Rock => Scissors,
                Paper => Rock,
                Scissors => Paper,
            },
        }
    }
}

#[cfg(test)]
mod play_outcome {
    use super::*;

    #[test]
    fn rock_beats_scissors() {
        let lhs = Hand::Rock;
        let rhs = Hand::Scissors;

        assert_eq!(lhs.play_round(rhs), Outcome::Win);
    }

    #[test]
    fn rock_draws_to_rock() {
        let lhs = Hand::Rock;
        let rhs = Hand::Rock;

        assert_eq!(lhs.play_round(rhs), Outcome::Draw);
    }

    #[test]
    fn rock_loses_to_paper() {
        let lhs = Hand::Rock;
        let rhs = Hand::Paper;

        assert_eq!(lhs.play_round(rhs), Outcome::Loss);
    }

    #[test]
    fn paper_beats_rock() {
        let lhs = Hand::Paper;
        let rhs = Hand::Rock;

        assert_eq!(lhs.play_round(rhs), Outcome::Win);
    }

    #[test]
    fn paper_draws_to_paper() {
        let lhs = Hand::Paper;
        let rhs = Hand::Paper;

        assert_eq!(lhs.play_round(rhs), Outcome::Draw);
    }

    #[test]
    fn paper_loses_to_scissors() {
        let lhs = Hand::Paper;
        let rhs = Hand::Scissors;

        assert_eq!(lhs.play_round(rhs), Outcome::Loss);
    }

    #[test]
    fn scissors_beats_paper() {
        let lhs = Hand::Scissors;
        let rhs = Hand::Paper;

        assert_eq!(lhs.play_round(rhs), Outcome::Win);
    }

    #[test]
    fn scissors_draws_to_scissors() {
        let lhs = Hand::Scissors;
        let rhs = Hand::Scissors;

        assert_eq!(lhs.play_round(rhs), Outcome::Draw);
    }

    #[test]
    fn scissors_loses_to_rock() {
        let lhs = Hand::Scissors;
        let rhs = Hand::Rock;

        assert_eq!(lhs.play_round(rhs), Outcome::Loss);
    }
}
