#![feature(test)]

mod rock_paper_scissors;

extern crate test;

use std::fs;

use rock_paper_scissors::{Hand, Outcome, Score};

const INPUT: &str = "src/bin/02/input.txt";

fn main() {
    let strategy_p1 = parse_input_wrong_encoding(INPUT);
    let score_p1 = play_rock_paper_scissors(&strategy_p1);
    println!("Total score problem 1: {score_p1}");

    let strategy_p2 = parse_input_correct_encoding(INPUT);
    let score_p2 = play_rock_paper_scissors(&strategy_p2);
    println!("Total score problem 2: {score_p2}");
}

fn play_rock_paper_scissors(strategy: &[(Hand, Hand)]) -> Score {
    let mut score = 0;
    for round in strategy {
        let hand_score = round.1.score();
        let round_score = round.1.play_round(round.0).score();
        score += hand_score + round_score;
    }
    score
}

fn parse_input_wrong_encoding(path: &str) -> Vec<(Hand, Hand)> {
    let mut rounds = Vec::new();
    fs::read_to_string(path)
        .expect("Failed to read file")
        .lines()
        .for_each(|line| {
            if let [encoded_opponent_hand, encoded_own_hand] =
                line.split_whitespace().collect::<Vec<_>>()[..]
            {
                rounds.push((
                    Hand::decode(encoded_opponent_hand),
                    Hand::decode(encoded_own_hand),
                ));
            } else {
                panic!("Failed to parse play");
            }
        });
    rounds
}

fn parse_input_correct_encoding(path: &str) -> Vec<(Hand, Hand)> {
    let mut rounds = Vec::new();
    fs::read_to_string(path)
        .expect("Failed to read file")
        .lines()
        .for_each(|line| {
            if let [encoded_opponent_hand, encoded_outcome] =
                line.split_whitespace().collect::<Vec<_>>()[..]
            {
                let opponent_hand = Hand::decode(encoded_opponent_hand);
                rounds.push((
                    opponent_hand,
                    Hand::from_opponent_hand_and_outcome(
                        opponent_hand,
                        Outcome::decode(encoded_outcome),
                    ),
                ));
            } else {
                panic!("Failed to parse play");
            }
        });
    rounds
}

#[cfg(test)]
mod play_test {
    use super::*;

    const TEST_INPUT: &str = "src/bin/02/test_input.txt";

    #[test]
    fn parsing_wrong_encoding() {
        let strategy = parse_input_wrong_encoding(TEST_INPUT);
        assert_eq!(strategy.len(), 3);

        use Hand::*;
        assert_eq!(strategy[0], (Rock, Paper));
        assert_eq!(strategy[1], (Paper, Rock));
        assert_eq!(strategy[2], (Scissors, Scissors));
    }

    #[test]
    fn parsing_correct_encoding() {
        let strategy = parse_input_correct_encoding(TEST_INPUT);
        assert_eq!(strategy.len(), 3);

        use Hand::*;
        assert_eq!(strategy[0], (Rock, Rock));
        assert_eq!(strategy[1], (Paper, Rock));
        assert_eq!(strategy[2], (Scissors, Rock));
    }

    #[test]
    fn example_play_wrong_encoding() {
        let strategy = parse_input_wrong_encoding(TEST_INPUT);

        assert_eq!(play_rock_paper_scissors(&strategy[0..1]), 8);
        assert_eq!(play_rock_paper_scissors(&strategy[1..2]), 1);
        assert_eq!(play_rock_paper_scissors(&strategy[2..3]), 6);
        assert_eq!(play_rock_paper_scissors(&strategy), 15);
    }

    #[test]
    fn example_play_correct_encoding() {
        let strategy = parse_input_correct_encoding(TEST_INPUT);

        assert_eq!(play_rock_paper_scissors(&strategy[0..1]), 4);
        assert_eq!(play_rock_paper_scissors(&strategy[1..2]), 1);
        assert_eq!(play_rock_paper_scissors(&strategy[2..3]), 7);
        assert_eq!(play_rock_paper_scissors(&strategy), 12);
    }

    #[test]
    fn problem1() {
        let strategy = parse_input_wrong_encoding(INPUT);
        let score = play_rock_paper_scissors(&strategy);
        assert_eq!(score, 15337);
    }

    #[test]
    fn problem2() {
        let strategy = parse_input_correct_encoding(INPUT);
        let score = play_rock_paper_scissors(&strategy);
        assert_eq!(score, 11696);
    }
}

#[cfg(test)]
mod bench_02 {
    use std::process::Termination;
    use test::Bencher;

    use super::*;

    #[bench]
    fn bench_main(b: &mut Bencher) -> impl Termination {
        b.iter(|| main());
    }
}
