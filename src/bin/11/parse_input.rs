use std::{cell::RefCell, fs};

use crate::monkey::Monkey;

pub const INPUT: &str = "src/bin/11/input.txt";
#[cfg(test)]
pub const TEST_INPUT: &str = "src/bin/11/test_input.txt";

pub fn parse_input(path: &str, worry_level_divisor: u64) -> Vec<RefCell<Monkey>> {
    let mut monkeys = fs::read_to_string(path)
        .expect("Failed to read file")
        .split("\n\n")
        .map(|s| Monkey::from_str(s, worry_level_divisor))
        .map(RefCell::new)
        .collect::<Vec<_>>();
    let monkey_divisor_product = monkeys
        .iter()
        .map(|m| m.borrow().test_remainder_divisor)
        .product::<u64>();
    for monkey in &mut monkeys {
        monkey.borrow_mut().all_monkeys_divisor_prod = monkey_divisor_product;
    }
    monkeys
}

#[cfg(test)]
mod tests {
    use crate::monkey::Operation;

    use super::*;

    #[test]
    fn test_parse_input() {
        let worry_level_divisor = 3;
        let monkeys = parse_input(TEST_INPUT, worry_level_divisor);
        assert_eq!(monkeys.len(), 4);

        let monkey_two = &monkeys[2];
        assert_eq!(monkey_two.borrow().items, vec![79, 60, 97]);
        assert_eq!(monkey_two.borrow().operation, Operation::MulSelf);
        assert_eq!((monkey_two.borrow().test)(12), 3);
        assert_eq!((monkey_two.borrow().test)(13), 1);
        assert_eq!((monkey_two.borrow().test)(14), 3);

        let monkey_three = &monkeys[3];
        assert_eq!(monkey_three.borrow().items, vec![74]);
        assert_eq!(monkey_three.borrow().operation, Operation::Add(3));
        assert_eq!((monkey_three.borrow().test)(16), 1);
        assert_eq!((monkey_three.borrow().test)(17), 0);
        assert_eq!((monkey_three.borrow().test)(18), 1);
    }
}
