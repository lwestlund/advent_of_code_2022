use std::cell::RefCell;

mod monkey;
mod parse_input;

use monkey::Monkey;
use parse_input::{parse_input, INPUT};

fn main() {
    {
        // Problem 1
        let worry_level_divisor = 3;
        let mut monkeys = parse_input(INPUT, worry_level_divisor);
        let num_rounds = 20;
        play_rounds(&mut monkeys, num_rounds);
        let monkey_business = compute_monkey_business(&monkeys);
        println!("Monkey business after {num_rounds} rounds: {monkey_business}");
    }

    {
        // Problem 2
        let worry_level_divisor = 1;
        let mut monkeys = parse_input(INPUT, worry_level_divisor);
        let num_rounds = 10_000;
        play_rounds(&mut monkeys, num_rounds);
        let monkey_business = compute_monkey_business(&monkeys);
        println!("Monkey business after {num_rounds} rounds: {monkey_business}");
    }
}

fn play_rounds(monkeys: &mut [RefCell<Monkey>], num_rounds: u64) {
    for _round in 0..num_rounds {
        play_round(monkeys);
    }
}

fn play_round(monkeys: &mut [RefCell<Monkey>]) {
    for monkey in monkeys.iter() {
        let items_zip_to_monkey = monkey.borrow_mut().take_turn();
        for (item, monkey_index) in items_zip_to_monkey {
            monkeys[monkey_index].borrow_mut().give_item(item);
        }
    }
}

fn compute_monkey_business(monkeys: &[RefCell<Monkey>]) -> u64 {
    let max_inspections = monkeys.iter().fold((0, 0), |(n1, n2), m| {
        let n = m.borrow().num_inspected_items();
        if n > n1 && n > n2 {
            (n, n1)
        } else if n > n2 {
            (n1, n)
        } else {
            (n1, n2)
        }
    });
    max_inspections.0 * max_inspections.1
}

#[cfg(test)]
mod tests {
    use crate::parse_input::TEST_INPUT;

    use super::*;

    #[test]
    fn test_data_problem1() {
        let worry_level_divisor = 3;
        let mut monkeys = parse_input(TEST_INPUT, worry_level_divisor);

        // Round 1
        play_round(&mut monkeys);
        assert_eq!(monkeys[0].borrow().list_items(), vec![20, 23, 27, 26]);
        assert_eq!(
            monkeys[1].borrow().list_items(),
            vec![2080, 25, 167, 207, 401, 1046]
        );
        assert_eq!(monkeys[2].borrow().list_items(), vec![]);
        assert_eq!(monkeys[3].borrow().list_items(), vec![]);

        // Round 2
        play_round(&mut monkeys);
        assert_eq!(
            monkeys[0].borrow().list_items(),
            vec![695, 10, 71, 135, 350]
        );
        assert_eq!(monkeys[1].borrow().list_items(), vec![43, 49, 58, 55, 362]);
        assert_eq!(monkeys[2].borrow().list_items(), vec![]);
        assert_eq!(monkeys[3].borrow().list_items(), vec![]);

        // Round 3
        play_round(&mut monkeys);
        assert_eq!(monkeys[0].borrow().list_items(), vec![16, 18, 21, 20, 122]);
        assert_eq!(
            monkeys[1].borrow().list_items(),
            vec![1468, 22, 150, 286, 739]
        );
        assert_eq!(monkeys[2].borrow().list_items(), vec![]);
        assert_eq!(monkeys[3].borrow().list_items(), vec![]);

        // Play until round 20.
        play_rounds(&mut monkeys, 17);
        assert_eq!(monkeys[0].borrow().list_items(), vec![10, 12, 14, 26, 34]);
        assert_eq!(
            monkeys[1].borrow().list_items(),
            vec![245, 93, 53, 199, 115]
        );
        assert_eq!(monkeys[2].borrow().list_items(), vec![]);
        assert_eq!(monkeys[3].borrow().list_items(), vec![]);

        assert_eq!(monkeys[0].borrow().num_inspected_items(), 101);
        assert_eq!(monkeys[1].borrow().num_inspected_items(), 95);
        assert_eq!(monkeys[2].borrow().num_inspected_items(), 7);
        assert_eq!(monkeys[3].borrow().num_inspected_items(), 105);

        let monkey_business = compute_monkey_business(&monkeys);
        assert_eq!(monkey_business, 10605);
    }

    #[test]
    fn test_data_problem2() {
        let worry_level_divisor = 1;
        let mut monkeys = parse_input(TEST_INPUT, worry_level_divisor);

        // Round 1
        play_round(&mut monkeys);
        assert_eq!(monkeys[0].borrow().num_inspected_items(), 2);
        assert_eq!(monkeys[1].borrow().num_inspected_items(), 4);
        assert_eq!(monkeys[2].borrow().num_inspected_items(), 3);
        assert_eq!(monkeys[3].borrow().num_inspected_items(), 6);

        // Play until round 20.
        play_rounds(&mut monkeys, 20 - 1);
        assert_eq!(monkeys[0].borrow().num_inspected_items(), 99);
        assert_eq!(monkeys[1].borrow().num_inspected_items(), 97);
        assert_eq!(monkeys[2].borrow().num_inspected_items(), 8);
        assert_eq!(monkeys[3].borrow().num_inspected_items(), 103);

        // Play until round 1000.
        play_rounds(&mut monkeys, 1000 - 20);
        assert_eq!(monkeys[0].borrow().num_inspected_items(), 5204);
        assert_eq!(monkeys[1].borrow().num_inspected_items(), 4792);
        assert_eq!(monkeys[2].borrow().num_inspected_items(), 199);
        assert_eq!(monkeys[3].borrow().num_inspected_items(), 5192);

        // Play until round 2000.
        play_rounds(&mut monkeys, 2000 - 1000);
        assert_eq!(monkeys[0].borrow().num_inspected_items(), 10419);
        assert_eq!(monkeys[1].borrow().num_inspected_items(), 9577);
        assert_eq!(monkeys[2].borrow().num_inspected_items(), 392);
        assert_eq!(monkeys[3].borrow().num_inspected_items(), 10391);

        // Play until round 10 000.
        play_rounds(&mut monkeys, 10000 - 2000);
        assert_eq!(monkeys[0].borrow().num_inspected_items(), 52166);
        assert_eq!(monkeys[1].borrow().num_inspected_items(), 47830);
        assert_eq!(monkeys[2].borrow().num_inspected_items(), 1938);
        assert_eq!(monkeys[3].borrow().num_inspected_items(), 52013);

        let monkey_business = compute_monkey_business(&monkeys);
        assert_eq!(monkey_business, 2_713_310_158);
    }

    #[test]
    fn test_problem1() {
        let mut monkeys = parse_input(INPUT, 3);
        play_rounds(&mut monkeys, 20);
        let monkey_business = compute_monkey_business(&monkeys);
        assert_eq!(monkey_business, 56120);
    }

    #[test]
    fn test_problem2() {
        let mut monkeys = parse_input(INPUT, 1);
        play_rounds(&mut monkeys, 10000);
        let monkey_business = compute_monkey_business(&monkeys);
        assert_eq!(monkey_business, 24389045529);
    }
}
