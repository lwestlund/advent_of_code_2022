pub struct Monkey {
    pub items: Vec<u64>,
    pub operation: Operation,
    pub test: Box<dyn Fn(u64) -> usize>,
    num_inspected_items: u64,
    worry_level_divisor: u64,
    pub test_remainder_divisor: u64,
    pub all_monkeys_divisor_prod: u64,
}

#[derive(Debug, PartialEq)]
pub enum Operation {
    Add(u64),
    Mul(u64),
    MulSelf,
}

impl Monkey {
    pub fn from_str(s: &str, worry_level_divisor: u64) -> Self {
        let lines = s.lines().collect::<Vec<_>>();
        let items = lines[1]
            .split(':')
            .skip(1)
            .collect::<String>()
            .split(',')
            .map(|s| s.trim().parse().unwrap())
            .collect::<Vec<_>>();
        let operation = {
            let things = lines[2].trim().split('=').collect::<Vec<_>>()[1]
                .split_whitespace()
                .collect::<Vec<_>>();
            match things[1] {
                "+" => Operation::Add(things[2].parse().unwrap()),
                "*" => {
                    let parsed = things[2].parse::<u64>();
                    if let Ok(parsed) = parsed {
                        Operation::Mul(parsed)
                    } else {
                        Operation::MulSelf
                    }
                }
                _ => unimplemented!("Unsupported operator"),
            }
        };
        let test_remainder_divisor = {
            let test_str = lines[3];
            test_str
                .split_whitespace()
                .last()
                .unwrap()
                .parse::<u64>()
                .unwrap()
        };
        let test = {
            let true_case_monkey = {
                let true_str = lines[4];
                true_str
                    .split_whitespace()
                    .last()
                    .unwrap()
                    .parse::<usize>()
                    .unwrap()
            };
            let false_case_monkey = {
                let false_str = lines[5];
                false_str
                    .split_whitespace()
                    .last()
                    .unwrap()
                    .parse::<usize>()
                    .unwrap()
            };
            Box::new(move |worry: u64| {
                if worry.rem_euclid(test_remainder_divisor) == 0 {
                    true_case_monkey
                } else {
                    false_case_monkey
                }
            })
        };

        Self {
            items,
            operation,
            test,
            num_inspected_items: 0,
            worry_level_divisor,
            test_remainder_divisor,
            all_monkeys_divisor_prod: 1,
        }
    }

    pub fn take_turn(&mut self) -> Vec<(u64, usize)> {
        self.items
            .drain(..)
            .map(|item_worry_level| {
                self.num_inspected_items += 1;
                let mut item_worry_level = match self.operation {
                    Operation::Add(v) => item_worry_level + v,
                    Operation::Mul(v) => item_worry_level * v,
                    Operation::MulSelf => item_worry_level * item_worry_level,
                };
                item_worry_level /= self.worry_level_divisor;
                item_worry_level = item_worry_level.rem_euclid(self.all_monkeys_divisor_prod);
                (item_worry_level, (self.test)(item_worry_level))
            })
            .collect()
    }

    pub fn give_item(&mut self, item: u64) {
        self.items.push(item);
    }

    #[cfg(test)]
    pub fn list_items(&self) -> &[u64] {
        &self.items
    }

    pub fn num_inspected_items(&self) -> u64 {
        self.num_inspected_items
    }
}

#[cfg(test)]
mod test_monkey {
    use super::*;

    #[test]
    fn test_from_str() {
        let s = "Monkey 0:\n\
                   Starting items: 79, 98\n\
                   Operation: new = old * 19\n\
                   Test: divisible by 23\n\
                     If true: throw to monkey 2\n\
                     If false: throw to monkey 3";
        let worry_level_divisor = 3;
        let monkey = Monkey::from_str(s, worry_level_divisor);
        assert_eq!(monkey.items, vec![79, 98]);
        assert_eq!(monkey.operation, Operation::Mul(19));
        assert_eq!((monkey.test)(22), 3);
        assert_eq!((monkey.test)(23), 2);
        assert_eq!((monkey.test)(24), 3);
        assert_eq!((monkey.test)(45), 3);
        assert_eq!((monkey.test)(46), 2);
        assert_eq!((monkey.test)(47), 3);
    }
}
