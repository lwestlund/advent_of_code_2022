use std::fs;

mod rope;

use crate::rope::{Direction, Move, Rope};

const INPUT: &str = "src/bin/09/input.txt";

fn main() {
    let moves = parse_input(INPUT);

    {
        // Problem 1
        let mut rope = Rope::new(2);
        let num_tail_positions = rope.count_unique_tail_positions_from_moves(&moves);
        println!("The number of unique tail positions with 2 knots is: {num_tail_positions}");
    }

    {
        // Problem 2
        let mut rope = Rope::new(10);
        let num_tail_positions = rope.count_unique_tail_positions_from_moves(&moves);
        println!("The number of unique tail positions with 10 knots is: {num_tail_positions}");
    }
}

fn parse_input(path: &str) -> Vec<Move> {
    fs::read_to_string(path)
        .expect("Failed to read file")
        .lines()
        .map(|line| {
            let parts = line.split_whitespace().collect::<Vec<_>>();
            use Direction::*;
            let direction = match parts[0] {
                "U" => Up,
                "D" => Down,
                "L" => Left,
                "R" => Right,
                d => panic!("Unknown direction {d}"),
            };
            let num_times = parts[1].parse::<u32>().unwrap();
            Move {
                direction,
                num_times,
            }
        })
        .collect()
}

#[cfg(test)]
mod position_tests {
    use super::*;

    const TEST_INPUT_1: &str = "src/bin/09/test_input_1.txt";
    const TEST_INPUT_2: &str = "src/bin/09/test_input_2.txt";

    #[test]
    fn test_data_1_problem1() {
        let moves = parse_input(TEST_INPUT_1);
        let mut rope = Rope::new(2);
        assert_eq!(rope.count_unique_tail_positions_from_moves(&moves), 13);
    }

    #[test]
    fn test_data_1_problem2() {
        let moves = parse_input(TEST_INPUT_1);
        let mut rope = Rope::new(10);
        assert_eq!(rope.count_unique_tail_positions_from_moves(&moves), 1);
    }

    #[test]
    fn test_data_2_problem2() {
        let moves = parse_input(TEST_INPUT_2);
        let mut rope = Rope::new(10);
        assert_eq!(rope.count_unique_tail_positions_from_moves(&moves), 36);
    }

    #[test]
    fn test_problem1() {
        let moves = parse_input(INPUT);
        let mut rope = Rope::new(2);
        assert_eq!(rope.count_unique_tail_positions_from_moves(&moves), 5874);
    }

    #[test]
    fn test_problem2() {
        let moves = parse_input(INPUT);
        let mut rope = Rope::new(10);
        assert_eq!(rope.count_unique_tail_positions_from_moves(&moves), 2467);
    }
}
