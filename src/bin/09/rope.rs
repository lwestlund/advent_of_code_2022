use std::collections::HashSet;

pub struct Rope {
    knot_positions: Vec<Position>,
    unique_tail_positions: HashSet<Position>,
}
impl Rope {
    pub fn new(num_knots: usize) -> Self {
        let mut knot_positions = Vec::with_capacity(num_knots);
        for _ in 0..num_knots {
            knot_positions.push(Position::new());
        }
        let mut unique_tail_positions = HashSet::new();
        unique_tail_positions.insert(*knot_positions.last().unwrap());
        Self {
            knot_positions,
            unique_tail_positions,
        }
    }
    pub fn count_unique_tail_positions_from_moves(&mut self, moves: &[Move]) -> usize {
        for m in moves {
            for _ in 0..m.num_times {
                let head = &mut self.knot_positions[0];
                match m.direction {
                    Direction::Up => head.move_up(),
                    Direction::Down => head.move_down(),
                    Direction::Left => head.move_left(),
                    Direction::Right => head.move_right(),
                }
                let num_knots = self.knot_positions.len();
                for i in 0..num_knots - 1 {
                    let leader_knot = self.knot_positions[i];
                    let follower_knot = &mut self.knot_positions[i + 1];
                    if !follower_knot.adjacent(&leader_knot) {
                        follower_knot.step_towards(&leader_knot);
                        let leader_knot_is_second_to_last = i == num_knots - 2;
                        if leader_knot_is_second_to_last {
                            // If the leader knot is the second to last knot, the follower knot
                            // is the last knot and we want to track its position.
                            self.unique_tail_positions.insert(*follower_knot);
                        }
                    }
                }
            }
        }
        self.unique_tail_positions.len()
    }
}

#[derive(PartialEq, Eq, Clone, Copy, Debug, Hash)]
struct Position {
    x: i32,
    y: i32,
}
impl Position {
    fn new() -> Self {
        Self { x: 0, y: 0 }
    }
    fn move_up(&mut self) {
        self.y += 1;
    }
    fn move_down(&mut self) {
        self.y -= 1;
    }
    fn move_left(&mut self) {
        self.x -= 1;
    }
    fn move_right(&mut self) {
        self.x += 1;
    }
    fn adjacent(&self, other: &Self) -> bool {
        (other.x - self.x).abs() <= 1 && (other.y - self.y).abs() <= 1
    }
    fn step_towards(&mut self, other: &Self) {
        let diff_x = other.x - self.x;
        let diff_y = other.y - self.y;
        if diff_x.abs() > 1 && diff_y.abs() == 1 {
            self.x += diff_x.signum();
            self.y = other.y;
        } else if diff_y.abs() > 1 && diff_x.abs() == 1 {
            self.x = other.x;
            self.y += diff_y.signum();
        } else {
            self.x += diff_x.signum();
            self.y += diff_y.signum();
        }
    }
}

pub struct Move {
    pub num_times: u32,
    pub direction: Direction,
}

pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[cfg(test)]
mod knot_tests {
    use super::*;

    #[test]
    fn test_adjacent() {
        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 0, y: 0 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 1, y: 0 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -1, y: 0 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 2, y: 0 };
        assert!(!p1.adjacent(&p2));
        assert!(!p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -2, y: 0 };
        assert!(!p1.adjacent(&p2));
        assert!(!p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 0, y: 1 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 0, y: -1 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 0, y: 2 };
        assert!(!p1.adjacent(&p2));
        assert!(!p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 0, y: -2 };
        assert!(!p1.adjacent(&p2));
        assert!(!p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 1, y: 1 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -1, y: 1 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -1, y: -1 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));

        let p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 1, y: -1 };
        assert!(p1.adjacent(&p2));
        assert!(p2.adjacent(&p1));
    }

    #[test]
    fn test_step_towards() {
        // Horizontal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 2, y: 0 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, 1);
        assert_eq!(p1.y, 0);

        // Horizontal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -2, y: 0 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, -1);
        assert_eq!(p1.y, 0);

        // Vertical
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 0, y: 2 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, 0);
        assert_eq!(p1.y, 1);

        // Vertical
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 0, y: -2 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, 0);
        assert_eq!(p1.y, -1);

        // Diagonal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 1, y: 2 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, 1);
        assert_eq!(p1.y, 1);

        // Diagonal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -1, y: 2 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, -1);
        assert_eq!(p1.y, 1);

        // Diagonal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 1, y: -2 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, 1);
        assert_eq!(p1.y, -1);

        // Diagonal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -1, y: -2 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, -1);
        assert_eq!(p1.y, -1);

        // Diagonal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 2, y: 1 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, 1);
        assert_eq!(p1.y, 1);

        // Diagonal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -2, y: 1 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, -1);
        assert_eq!(p1.y, 1);

        // Diagonal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: 2, y: -1 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, 1);
        assert_eq!(p1.y, -1);

        // Diagonal
        let mut p1 = Position { x: 0, y: 0 };
        let p2 = Position { x: -2, y: -1 };
        p1.step_towards(&p2);
        assert_eq!(p1.x, -1);
        assert_eq!(p1.y, -1);
    }
}
